---
layout: handbook-page-toc
title: Incubation Engineer's Playbook
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Incubation Engineer's Playbook

This playbook contains useful tips and tricks an Incubation Engineer may employ for SEG and project success.

### Converting an incubation backlog project to an active SEG

In order to maintain consistency, the process below should be followed when starting a new SEG from an incubation backlog project:

1. Create a new subgroup within the [Incubation Engineering group](https://gitlab.com/gitlab-org/incubation-engineering/). For example: [mobile-devops](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops)
2. Create a `meta` project within that new group. For example: [mlops/meta](https://gitlab.com/gitlab-org/incubation-engineering/mlops/meta)
3. Create a weekly update issue in that new `meta` project. This issue will replace the existing backlog project issue and be used to post weekly demo recordings from the new SEG. For example: [jamstack](https://gitlab.com/gitlab-org/incubation-engineering/jamstack/meta/-/issues/5)
4. Add all labels from the original backlog issue to the issue you just created. For example: [original issue](https://gitlab.com/gitlab-org/gitlab/-/issues/329592) | [new issue](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/readme/-/issues/7)
5. Add the new project to the `seg_issues_list` in the handbook direction generator module in the handbook. See this [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/93485/diffs) for an example.
6. And lastly, close out the original backlog issue with a comment pointing to the new weekly update issue ([example](https://gitlab.com/gitlab-org/gitlab/-/issues/329592#note_676451924)).

### Shadowing Field Marketing Events

Incubation Engineering team members benefit from shadowing customer / prospect events. This is aligned with the FY22-Q4 Engineering OKR "Increase focus on customer results..".

Field marketing has welcomed all Incubation Engineering team members to shadow upcoming field marketing events. These events are conducted with new prospects, existing customers and individual GitLab / DevOps enthusiasts.

Incubation Engineering team members may discover issues for upcoming events, and may request an invitation by commenting in the event's issue. 

The following issue boards are to be used to discover upcoming events:

- [Field & Corporate, EMEA](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933459?label_name[]=EMEA)
- [Field & Corporate, Central Europe](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1438243?label_name[]=Central%20Europe&label_name[]=EMEA)
- [Field & Corporate, UK/I](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1438265?label_name[]=UK%2FI)

Highlighting EMEA because majority of Incubation Engineers reside here. Other regions can be discovered in the Boards dropdown by searching for "Field & Corporate {region_name}".

Additionally, Field Marketing has active Slack channels where events are planned and prepped for. If useful, please consider joining:

- #emea_northern_europe_fieldmarketing
- #emea_central_europe_fieldmarketing
- #emea_southern_europe_fieldmarketing

...on Slack. Other relevant channels may be discovered via Slack search.

### New to prospect / customer conversations?

If the Incubation Engineering team member is not familiar with external conversations, the golden princple to remember is that the only thing that is expected from us is to _shadow_ the call. It is perfectly okay to say nothing and stay muted throughout the call. Introduce yourself if invited to. And help GitLab team members by keeping notes.

For those comfortable with external interactions, they may offer their services to the Field Marketing event lead by means of answering questions and interacting with the participants when the GitLab speaker requests support.

### Quality & Support Guidelines for Incubation Projects

- Is your feature behind a feature flag? It's preferrable that it is.
- Can your feature be visually labeled as an `incubation` or `experimental` feature without distracting / taking away from the GitLab user experience?
- Error messages  / stack traces / logs may indicate that this is an `incubation` feature.
- Before feature flag roll-out, is the feature documented in sufficient detail? Documentation is the first resource for users and support agents working with the feature.
- Pro-actively reach out and make introductions before feature roll-out / general availability.

### Security Guidelines for Incubation Projects

Incubation Engineers should familiarize themselves with the [GitLab AppSec Review](/handbook/engineering/security/security-engineering-and-research/application-security/appsec-reviews.html) process and preferably have the AppSec review triggered early for each merge request when appropriate.

Incubation Engineers are often required to create prototypes or demo applications as they are iterating on ideas and gathering feedback. Below are some security guidelines to keep in mind while building these applications:

- Code for prototypes and demo projects should be hosted in the Incubation Engineering specific `gitlab-incubation-engineering-demos`. This is preferred because projects in a subgroup will inherit configurations from the parent group ([group access tokens](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html) or [group CI variables](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-group) for example), which can result in unintended behavior when an project hasn't accounted for the inherited configurations appropriately.
- Prototypes and demo projects should follow GitLab's [Secure Coding Guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html).
- Use of [Personal Access Tokens is discouraged](/handbook/security/#personal-access-tokens), Project Access Tokens are often sufficient.
